use std::io::Read;
use std::net::{TcpListener, TcpStream};
use std::thread;

fn handle_client(mut stream: TcpStream) {
    let mut buf = [0; 1024];
    match stream.read(&mut buf) {
        Ok(n) => {
            if n == 0 {
                // Connection was closed
                return;
            }
            let mut data = String::from_utf8_lossy(&buf[0..n]).to_string();
            while data.ends_with('\n') || data.ends_with('\r') {
                data.pop();
            }
            println!("Received: {}", data);

            if data == "exit" {
                std::process::exit(0);
            }
        }
        Err(err) => {
            eprintln!("An error occurred while reading: {}", err);
        }
    }
}

fn main() {
    let listener = TcpListener::bind("127.0.0.1:8080").expect("Could not bind to address");
    println!("Server listening on 127.0.0.1:8080");

    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                thread::spawn(|| {
                    handle_client(stream);
                });
            }
            Err(err) => {
                eprintln!("An error occurred while accepting connection: {}", err);
            }
        }
    }
}

